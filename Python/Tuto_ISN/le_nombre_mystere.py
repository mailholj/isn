#But du jeu: Trouver le nombre caché en un minimum de coups
#Bonus : mode deux joueurs, gestion de plusieurs niveaux de difficultés, fonction rejouer


from random import *

#fonction nombre de joueur
def initialisation_nbr_joueur ():
    
    nbr_de_joueur = ''
    print ("Veuillez définir si vous jouer seul ou à 2.\n")
    list_nbr_max = ['100', '1000', '10000']
    range_nbr_joueur = ['1', '2']


    while nbr_de_joueur == '' :

        nbr_de_joueur = input ("nombre de joueur ? Tapez 1 ou 2 .\n")

        if nbr_de_joueur not in range_nbr_joueur:
            
           nbr_de_joueur = ''
           print ("Nombre de joueur incorrect. Veuillez recommencer.\n")    

    return int(nbr_de_joueur)

#fonction pour générer les différentes difficultés
def intitialisation_difficulte () :

    difficulte = 0
    nbr_max = 0
    list_nbr_max = [100, 1000, 10000]
    range_difficulte = [1, 2, 3]
    
    
    while difficulte == 0:
    
        print ("Veuillez definir une difficult:\n")
        print ("Taper 1 pour un intervalle de 0 à 100")
        print ("Taper 2 pour un intervalle de 0 à 1000")
        print ("Taper 3 pour un intervalle de 0 à 10000")

        difficulte = int(input ("\nChoisissez un intervalle : "))

        if difficulte not in range_difficulte:
                                            
             print ("\nVeuillez rentrer un intervalle proposé\n")
             difficulte = 0

    
    nbr_max = list_nbr_max[difficulte-1]


    return nbr_max

#fonction pour definir le nombre à trouver
#définis le nombre à trouver de façon aléatoire si un seul joueur
#si deux joueurs on demande à un premier de definir un nombre dans l'intervalle choisis 
def intialisation_nbr_a_trouver (nbr_de_joueur, nbr_max) :
    ''' Intitialisation nombre à trouver
'''

    nbr_a_trouver = 0
    
    if nbr_de_joueur == 2:

        while nbr_a_trouver == 0:
        
            print ("Le premier joueur va entrer un nombre que le deuxième joueur devra trouver\n")
            nbr_a_trouver = int(input ("Premier joueur entrez un nombre dans l'intervalle de 0 à " + str(nbr_max) + " : "))

            if nbr_a_trouver < 1 :

                print ("\nVeuillez entrer un nombre dans l'écart choisis\n")
                nbr_a_trouver = 0

            if nbr_a_trouver > nbr_max:

                print ("\nVeuillez entrer un nombre dans l'écart choisis\n")
                nbr_a_trouver = 0
                
        print ("\n" * 100)
        
    else:

        nbr_a_trouver = randint(1,nbr_max)

    return nbr_a_trouver

#fonction tour du joueur 
def tour_du_joueur (nbr_de_joueur,nbr_a_trouver) :
    ''' Tour du joueur
    '''
    nbr_de_tour = 0
    nbr_propose = 0
    
    if nbr_de_joueur == 2:

        print ("\nLe deuxième joueur peut chercher le juste prix")

    else:
        print ("\nVous pouvez chercher le juste prix")

    print ("GO !")

    while nbr_propose != nbr_a_trouver:

        nbr_propose = int(input ("Propoez un nombre : "))
        nbr_de_tour = nbr_de_tour + 1

        if nbr_propose > nbr_a_trouver:

            print ("C'est moins.\n")

        if nbr_propose < nbr_a_trouver:

            print ("C'est plus.\n")


    print ("\nBravo c'est gagn�")
    print ("Vous avez trouvé en " + str(nbr_de_tour) + " coups.")

    return nbr_de_tour



#Début main

nbr_a_trouver = 0
nbr_de_tour = 0
nbr_max = 0
nbr_de_joueur = 0
rejouer = True

while rejouer == True:

    print ("Bonjour à tous, vous êtes là pour jouer au juste prix.\n")
    
    nbr_de_joueur = initialisation_nbr_joueur ()
    nbr_max = intitialisation_difficulte ()
    nbr_a_trouver = intialisation_nbr_a_trouver (nbr_de_joueur, nbr_max)
    nbr_de_tour = tour_du_joueur (nbr_de_joueur,nbr_a_trouver)
    nbr_rejouer = 0
    range_nbr_rejouer = [1, 2]
    
    print ("\nVoulez-vous rejouer ?\n")

            
    while nbr_rejouer == 0:
    
        print ("Tapez 1 pour OUI.")
        print ("Tapez 2 pour NON.")
        nbr_rejouer = int(input (""))

        if nbr_rejouer not in range_nbr_rejouer:

            print ("Entrez une valeur proposée.")
            nbr_rejouer = 0


    if nbr_rejouer == 1:

        rejouer = True

    else :

        rejouer = False

print ("\nMerci d'avoir jouer.")
    
    

