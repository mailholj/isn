from tkinter import *
from random import randint


class Application(Frame):

    # la classe qui englobe le jeu

    def __init__(self, master):
        
        #initialisation de la fenêtre et du jeu

        Frame.__init__(self, master)
        master.minsize(width=500, height=200)
        self.grid()

        self.creation_element()
        self.intitialisation_value()
        self.varLabel1.set("Bonjour à tous, vous êtes là pour jouer au juste prix.")
        self.varLabel2.set("Veuillez définir si vous jouer avec 1 joueur ou 2 joueurs.")


          
                  
    def creation_element(self):

        #création des éléments de la fenêtre

        self.varLabel1 = StringVar()
        Label(self, textvariable = self.varLabel1).grid(row = 0, column = 0, columnspan = 2, sticky = W)

        self.varLabel2 = StringVar()
        Label(self,textvariable = self.varLabel2).grid(row = 1, column = 0, columnspan = 2, sticky = W)

        self.reponse = Entry(self)
        self.reponse.grid(row=2, column=1, sticky=W)

        self.varButton = StringVar()
        self.Button1 = Button(self, textvariable = self.varButton)
        self.Button1.grid(row=2, column=2, columnspan=4, sticky=W)
        self.Button1.bind('<Button-1>', self.jeu)

        # permet l'usage de la touche entrée

        self.master.bind('<Return>', self.jeu)

        self.results_txt = Text(self, width=40, height=5,wrap=WORD)
        self.results_txt.grid(row = 3, column = 0, columnspan = 3)


    def intitialisation_value (self):

        #initialisation des variables et réinitialisation pour rejouer

        self.varLabel1.set("Bonjour à tous, vous êtes là pour jouer au Juste Prix!")
        self.varLabel2.set("Veuillez définir si vous jouer avec 1 joueur ou 2 joueurs.")
        self.varButton.set("Enter")
        self.nbr_de_joueur = 0
        self.difficulte = 0
        self.nbr_a_trouver = 0
        self.gagne = False
        self.nbr_de_tour = 0
        self.nbr_de_tour_p2 = 0
        self.nbr_propose = 0
        self.range_nbr_joueur = [1, 2]
        self.nbr_max = 0
        self.list_nbr_max = [100, 1000, 10000]
        self.range_difficulte = [1, 2, 3]
        self.list_chiffre = ['0', '1', '2', '3', '4','5', '6', '7', '8', '9']
        self.message("")
        self.nbr_trouve = 0
        self.echanger = True

    
    def jeu(self, Event):

        #action à faire lorsque le boutton est cliqué

        # définis le nbr de joueur
                
        if self.nbr_de_joueur == 0:

            self.initialisation_nbr_joueur()
        
        else:
            
            # définis la difficulté

            if self.difficulte == 0:
                
                
                self.intitialisation_difficulte()

            else:
                
                # définis le nbr à trouver

                if self.nbr_a_trouver == 0:
                    
                    # transition round 1 --> round 2

                    if self.echanger == False:
                        
                        self.echanger = True
                        self.varButton.set('Enter')
                        self.varLabel1.set("Les rôles sont échangés.")
                        self.varLabel2.set("J2 va entrer un nombre que J1 devra trouver.")
                        self.message("")

                    else:

                        self.intialisation_nbr_a_trouver()

                else:

                    # joue le tour du joueur jusqu'à ce qu'il trouve

                    if self.gagne == False:

                        self.tour_du_joueur()
                    
                    else:

                        # si le nombre est trouvé (1 fois pour 1 joueur et 2 fois pour 2 joueur)
                        # propose de rejouer

                        if self.nbr_trouve == 2:
                            
                            self.rejouer()
                                

    def initialisation_nbr_joueur (self):
        
        #initialise le nombre de joueur

        self.reponse_donnee()


        self.varLabel2.set("Veuillez définir si vous jouer avec 1 joueur ou 2 joueurs.")
        
        self.nbr_de_joueur = self.entree
            
        # vérifier si la valeur entrée est dans l'intervalle

        if self.nbr_de_joueur not in self.range_nbr_joueur:
            
            self.nbr_de_joueur = 0
            self.message("Nombre de joueur incorrect. Veuillez recommencer.")
        
        # transition initialisation difficulte

        else:

            self.message("")
            self.varLabel2.set("Veuillez definir une difficulté:\nTaper 1 pour un intervalle de 0 à 100\nTaper 2 pour un intervalle de 0 à 1000\nTaper 3 pour un intervalle de 0 à 10000")


    
    def intitialisation_difficulte (self) :
        
        #initialisation de la difficulté

        self.reponse_donnee()
              

        self.difficulte = self.entree

        # vérifier si la valeur entrée est dans l'intervalle

        if self.difficulte not in self.range_difficulte:
                                            
            self.message("Veuillez rentrer un intervalle proposé.")
            self.difficulte = 0
        

        else:

            # définir le nbr max

            self.nbr_max = self.list_nbr_max[self.difficulte-1]
            
            # affichage 1 joueur

            if self.nbr_de_joueur == 1:
                
                self.message("")
                self.varLabel1.set("\nVous pouvez chercher le juste prix.")
                self.varLabel2.set("GO !")

                self.intialisation_nbr_a_trouver()
            
            # affichage 2 joueurs

            else:
                self.message("")
                self.varLabel2.set("J1 va entrer un nombre que J2 devra trouver.")




    def intialisation_nbr_a_trouver (self) :

        # Intitialisation nombre à trouver

        self.reponse_donnee()

        # si 2 joueurs

        if self.nbr_de_joueur == 2:

            # round 1
            
            if self.nbr_trouve == 0:
                self.varLabel2.set("J1 va entrer un nombre que J2 devra trouver.")
                self.nbr_a_trouver = self.entree
            
            # round 2

            if self.nbr_trouve == 1:
                self.varLabel2.set("J2 va entrer un nombre que J1 devra trouver.")
                self.nbr_a_trouver = self.entree

            # vérifier si la valeur entrée est dans l'intervalle

            if self.nbr_a_trouver < 1 or self.nbr_a_trouver > self.nbr_max:
                                  
                self.message("Veuillez entrer un nombre dans l'écart choisis. ( 1 à " + str(self.nbr_max) + ")")
                self.nbr_a_trouver = 0

            else:

                # round 1

                if self.nbr_trouve == 0:

                    self.message("")
                    self.varLabel1.set("J2 peut chercher le juste prix.")
                    self.varLabel2.set("GO !")
                
                #round 2

                if self.nbr_trouve == 1:

                    self.message("")
                    self.varLabel1.set("J1 peut chercher le juste prix.")
                    self.varLabel2.set("GO !")
                
        else:

            # si 1 joueur

            self.nbr_a_trouver = randint(1,self.nbr_max)


    def tour_du_joueur (self) :
        
        # test si nbr proposé correspond à celui qui est à trouver

        self.reponse_donnee()
        self.nbr_propose = self.entree

        # vérifier que la valeur entrée est dans l'intervalle et si elle ne l'est pas, on ne compte pas un coups et on redonne l'intervalle

        if self.nbr_propose < 1 or self.nbr_propose > self.nbr_max:

            self.message("Veuillez entrer un nombre dans l'écart choisis. ( 1 à " + str(self.nbr_max) + ")")
            return

        else:

            self.nbr_de_tour = self.nbr_de_tour + 1


        
        # nbr proposé inférieux ou supérieur

        if self.nbr_propose > self.nbr_a_trouver:

            self.message("C'est moins. (" + str(self.nbr_propose) + ")")

        if self.nbr_propose < self.nbr_a_trouver:

            self.message("C'est plus. (" + str(self.nbr_propose) + ")")

        # le joueur a trouver

        if self.nbr_propose == self.nbr_a_trouver:

            # si 1 joueur 

            if self.nbr_de_joueur == 1:

                self.gagne = True
                self.message("Trouvé. (" + str(self.nbr_propose) + ")\nPour rejouer cliquer sur rejouer")
                self.varLabel1.set("Bravo c'est gagné!")
                self.varLabel2.set("J2 a trouvé en " + str(self.nbr_de_tour) + " coups.")
                self.varButton.set("Rejouer")
                self.nbr_trouve = 2
            
            # si 2 joueurs

            else:

                # si round 1

                if self.nbr_trouve == 0 :

                    self.nbr_de_tour_p2 = self.nbr_de_tour
                    self.nbr_trouve = 1
                    self.message("Trouvé. (" + str(self.nbr_propose) + ")" + '\nCliquer sur "Echanger" quand vous êtes prêt.')
                    self.varLabel1.set("Vous allez maintenant échanger les rôles.")
                    self.varLabel2.set("J1 a trouvé en " + str(self.nbr_de_tour) + " coups.")
                    self.varButton.set("Echanger")
                    self.second_round_two_players()
                    
                    return

                # si round 2

                if self.nbr_trouve == 1:

                    self.nbr_trouve = 2
                    self.gagne = True

                    # si J1 a fait moins de coups que J2

                    if self.nbr_de_tour < self.nbr_de_tour_p2:

                        self.varLabel1.set("Bravo ! J1 a gagné !")
                        self.varLabel2.set("J1 = " + str(self.nbr_de_tour) + " coup(s) et J2 = " + str(self.nbr_de_tour_p2) + " coup(s)")

                    # si inverse

                    if self.nbr_de_tour_p2 < self.nbr_de_tour:

                        self.varLabel1.set("Bravo ! J2 a gagné !")
                        self.varLabel2.set("J2 = " + str(self.nbr_de_tour_p2) + " coup(s) et J1 = " + str(self.nbr_de_tour) + " coup(s)")
                    

                    self.message("Trouvé. (" + str(self.nbr_propose) + ")\n\nFin du mode 2 joeurs : \nPour rejouer cliquer sur rejouer")
                    self.varButton.set("Rejouer")




    def second_round_two_players (self):

        # initialise deuxième tour pour mode 2 joueurs

        self.nbr_a_trouver = 0
        self.nbr_de_tour = 0
        self.echanger = False

    
    def rejouer (self):
        
        #function rejouer; reinitialise les valeurs

        self.intitialisation_value()


    def reponse_donnee (self) :

        #récupère les valeurs dans Entry

        # si l'entrée est vide : fin de la fonction

        if self.reponse.get() == "":
            return

        # essaye d'obtenir une valeur int() de l'entrée

        try:
            self.entree = int((self.reponse.get()))

        # si impossible d'obtenir la valeur int() à partir d'entrée, renvoye un erreur

        except ValueError:
            self.message("Erreur d'entrée! (" + self.reponse.get()[0:10] + ")")
            self.reponse.delete(0, 'end')

        # clear la zone d'entrée
        else:
            self.reponse.delete(0, 'end')

    def message (self, message) : 

        #Supprime le contenu de la boite txt et affiche le message donné

        self.results_txt.delete(0.0, END)
        self.results_txt.insert(0.0, message)
            

def main ():

    #main fonction

    root = Tk()
    root.title("Juste Prix")
    app = Application(root)
    root.mainloop()
    app.mainloop()

# lance le programme

main()
