from tkinter import *
from random import randint



class Application(Frame):
    def __init__(self, master):

        Frame.__init__(self, master)
        master.minsize(width=500, height=200)
        self.grid()

        self.creation_element()
        
        

        self.nbr_a_trouver = 0
        self.nbr_de_tour = 0
        self.nbr_max = 0
        self.nbr_de_joueur = 0
        self.rejouer = True

        while self.rejouer == True:

    
            self.varLabel1.set("Bonjour à tous, vous êtes là pour jouer au juste prix.")      
        

            self.initialisation_nbr_joueur ()
            self.intitialisation_difficulte ()
            self.intialisation_nbr_a_trouver ()
            self.tour_du_joueur ()
            self.nbr_rejouer = 0
            self.range_nbr_rejouer = [1, 2]
        
            print ("\nVoulez-vous rejouer ?\n")

                    
            while self.nbr_rejouer == 0:
            
                print ("Tapez 1 pour OUI.")
                print ("Tapez 2 pour NON.")
                self.nbr_rejouer = int(input (""))

                if self.nbr_rejouer not in self.range_nbr_rejouer:

                    print ("Entrez une valeur proposée.")
                    self.nbr_rejouer = 0


            if self.nbr_rejouer == 1:

                self.rejouer = True

            else :

                self.rejouer = False
                print ("\nMerci d'avoir jouer.")
                  
    def creation_element(self):
        self.varLabel1 = StringVar()
        Label(self, textvariable = self.varLabel1).grid(row = 0, column = 0, columnspan = 2, sticky = W)

        self.varLabel2 = StringVar()
        Label(self,textvariable = self.varLabel2).grid(row = 1, column = 0, columnspan = 2, sticky = W)

        self.reponse = Entry(self)
        Button(self, text = 'Enter', command = self.reponse_donnée)

    def initialisation_nbr_joueur (self):
    
        self.nbr_de_joueur = 0
        self.varLabel2.set("Veuillez définir si vous jouer seul ou à 2.")
        self.range_nbr_joueur = [1, 2]

        while self.nbr_de_joueur == 0 :

            self.nbr_de_joueur = self.reponse_donnée
            
            self.varLabel2.set("Veuillez définir si vous jouer seul ou à 2.")

            if self.nbr_de_joueur not in self.range_nbr_joueur:
            
                self.nbr_de_joueur = 0
                self.varLabel2.set("Nombre de joueur incorrect. Veuillez recommencer.")    

        


    def reponse_donnée (self) :
        try:
            self.entree = int(self.reponse.get())
            self.reponse.select_clear()
            return self.entree

        except(ValueError):
            self.varLabel2.set("Entrée invalide")
            self.reponse.select_clear()



    def intitialisation_difficulte (self) :

        self.difficulte = 0
        self.nbr_max = 0
        self.list_nbr_max = [100, 1000, 10000]
        self.range_difficulte = [1, 2, 3]
    
    
        while self.difficulte == 0:
            
            self.varLabel2.set("Veuillez definir une difficult:\nTaper 1 pour un intervalle de 0 à 100\nTaper 2 pour un intervalle de 0 à 1000\nTaper 3 pour un intervalle de 0 à 10000")

            self.difficulte = self.entree

            if self.difficulte not in self.range_difficulte:
                                            
                self.varLabel2.set("Veuillez rentrer un intervalle proposé.")
                self.difficulte = 0

    
        self.nbr_max = self.list_nbr_max[self.difficulte-1]




    def intialisation_nbr_a_trouver (self) :
        ''' Intitialisation nombre à trouver
    '''

        self.nbr_a_trouver = 0
    
        if self.nbr_de_joueur == 2:

            while self.nbr_a_trouver == 0:
        
                self.varLabel1.set("Le premier joueur va entrer un nombre que le deuxième joueur devra trouver\n")
                self.nbr_a_trouver = self.entree

                if self.nbr_a_trouver < 1 or self.nbr_a_trouver > self.nbr_max:

                    self.varLabel2.set("\nVeuillez entrer un nombre dans l'écart choisis\n")
                    self.nbr_a_trouver = 0
                
            
        
        else:

            self.nbr_a_trouver = randint(1,self.nbr_max)


    def tour_du_joueur (self) :
        ''' Tour du joueur
    '''
        self.nbr_de_tour = 0
        self.nbr_propose = 0
    
        if self.nbr_de_joueur == 2:

            self.varLabel1.set("\nLe deuxième joueur peut chercher le juste prix")

        else:
            self.varLabel1.set("\nVous pouvez chercher le juste prix")

        self.varLabel2.set("GO !")

        while self.nbr_propose != self.nbr_a_trouver:

            self.nbr_propose = self.entree
            self.nbr_de_tour = self.nbr_de_tour + 1

            if self.nbr_propose > self.nbr_a_trouver:

                self.varLabel2.set("C'est moins.")

            if self.nbr_propose < self.nbr_a_trouver:

                self.varLabel2.set("C'est plus.")


        self.varLabel1.set("Bravo c'est gagné")
        self.varLabel2.set("Vous avez trouvé en " + str(self.nbr_de_tour) + " coups.")


def main ():
    root = Tk()
    root.title("Juste Prix")
    app = Application(root)
    root.mainloop()
    app.mainloop()

main()


    
    





































