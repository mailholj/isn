# -*- coding: utf-8 -*-

class app () :

    def __init__ (self) :
        
        i = 0

        while i == 0 :
            try :
                self.v1 = int(input ("Entrer le volume du plus petit verre doseur : "))
                self.v2 = int(input ("Entrer le volume du plus grand verre doseur : "))
                i = 1

            except :
                print("Erreur d'entrée")

        self.vcal = 0
        self.list_vol = []
        self.verif_plus_petit()
 

    def verif_plus_petit (self) :

        if self.v2 < self.v1 : 
            a= self.v2
            self.v2 = self.v1
            self.v1 = a

        self.cal_vol()

    def cal_vol (self) :

        while self.vcal != self.v2 :

            self.vcal = self.vcal + self.v1

            if self.vcal > self.v2 :
                self.vcal = self.vcal - self.v2           

            if self.vcal not in self.list_vol :
                self.list_vol.append(self.vcal)

            self.list_vol.sort()

        print("Les volumes possibles sont : ", self.list_vol)

if __name__ == '__main__' :

    app()