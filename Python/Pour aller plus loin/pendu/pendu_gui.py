from random import choice
import json
from tkinter import *

#!/usr/bin/python
# -*- coding: <utf-8> -*-

class Application (Frame):

    # le jeu en lui même

    def __init__ (self, master):

        Frame.__init__(self, master)
        master.minsize(width=500, height=200)
        self.grid()

        self.creation_element()
        self.initialisation_valeur()


    def creation_element(self):

        #création des éléments de la fenêtre

        self.varLabel1 = StringVar()
        Label(self, textvariable = self.varLabel1).grid(row = 0, column = 0, columnspan = 2, sticky = W)

        self.varLabel2 = StringVar()
        Label(self,textvariable = self.varLabel2).grid(row = 1, column = 0, columnspan = 2, sticky = W)

        self.reponse = Entry(self)
        self.reponse.grid(row=2, column=1, sticky=W)

        self.varButton = StringVar()
        self.Button1 = Button(self, textvariable = self.varButton)
        self.Button1.grid(row=2, column=2, columnspan=4, sticky=W)
        self.Button1.bind('<Button-1>', self.jeu)

        # permet l'usage de la touche entrée

        self.master.bind('<Return>', self.jeu)

        self.results_txt = Text(self, width=40, height=5,wrap=WORD)
        self.results_txt.grid(row = 3, column = 0, columnspan = 4)

    
    def initialisation_valeur (self) :

        self.nbr_de_joueur = 0
        self.range_nbr_joueur = [1, 2]
        self.difficulte = 0
        self.nbr_erreur_max = 0
        self.list_nbr_erreur_max = [4, 8, 10]
        self.range_difficulte = [1, 2, 3]
        self.value_error_mot = ['1','2','3','4','5','6','7','8','9',' ','é','è','ç','ê','î','â','ù','û',]
        self.error = False
        self.mot_a_trouver = ''
        self.donnees_file = open('C:/Users/Arcarox/Desktop/Mes Documents/ISN/Pour aller plus loin/pendu/donnees.json', 'r')
        self.data = json.load(self.donnees_file)
        self.list_mots = self.data['list_mots']
        self.donnees_file.closed
        self.mot_cache = list()
        self.lettre_proposee = ""
        self.lettre_proposee_dans_mot = False
        self.nbr_erreur = 0
        self.mot_cache_affiche = ''
        self.rejouer = 0
        self.mot_ajoute = ''
        self.entree = ""
        self.gagne = ""
        self.varButton.set("Enter")
        self.varLabel1.set("Bonjour à tous, vous êtes là pour jouer au Pendu!")
        self.varLabel2.set("Veuillez définir si vous jouer avec 1 joueur ou à plusieurs." + "\nEntrer 1 pour jouer seul et entrer 2 pour jouer à plusieurs.")
        self.message("")
        self.lettrre_entree = list()
                

    def jeu(self, Event):

        #action à faire lorsque le boutton est cliqué

        # définis le nbr de joueur
                
        if self.nbr_de_joueur == 0:

            self.initialisation_nbr_joueur()
        
        else:
            
            # définis la difficulté

            if self.difficulte == 0:
                
                
                self.intitialisation_difficulte()

            else:
                
                # définis le mot à trouver

                if self.mot_a_trouver == '':
                    
                   self.initialisation_mot_a_trouver()

                else:

                    # joue le tour du joueur jusqu'à ce qu'il trouve

                    if self.gagne == "":

                        self.tour_joueur()
                    
                    else:

                        #
                        if self.nbr_de_joueur == 2 :
                            
                            if self.mot_ajoute == "":
                            
                                self.ask_mot_ajoute()
                        
                            else:

                                self.fonction_rejouer()
                        
                        else :

                            self.fonction_rejouer()



    def initialisation_nbr_joueur (self):
        
        #initialise le nombre de joueur

        self.reponse_donnee()
        
        try:
            self.nbr_de_joueur = int(self.entree)

        # si impossible d'obtenir la valeur int() à partir d'entrée, renvoye un erreur

        except ValueError:
            self.message("Erreur d'entrée! (" + self.entree[0:10] + ")")
 
            
            
        # vérifier si la valeur entrée est dans l'intervalle

        if self.nbr_de_joueur not in self.range_nbr_joueur:
            
            self.nbr_de_joueur = 0
            self.message("Erreur d'entrée! (" + self.entree[0:10] + ")")
        
        # transition initialisation difficulte

        else:

            self.message("")
            self.varLabel2.set("Veuillez definir une difficulté:\nTaper 1 pour une limite de 4 erreurs.\nTaper 2 pour une limite de 8 erreurs.\nTaper 3 pour une limite de 10 erreurs.")





    def intitialisation_difficulte (self) :
        
        #initialisation de la difficulté

        self.reponse_donnee()
              

        try:
            self.difficulte = int(self.entree)

        # si impossible d'obtenir la valeur int() à partir d'entrée, renvoye un erreur

        except ValueError:
            self.message("Erreur d'entrée! (" + self.entree[0:10] + ")")



        # vérifier si la valeur entrée est dans l'intervalle

        if self.difficulte not in self.range_difficulte:
                                            
            self.message("Veuillez rentrer un intervalle proposé. (1, 2 ou 3)")
            self.difficulte = 0
        

        else:

            # définir le nbr d'erreur max

            self.nbr_erreur_max = self.list_nbr_erreur_max[self.difficulte-1]
            
            # affichage 1 joueur

            if self.nbr_de_joueur == 1:
                
                self.message("")
                self.varLabel1.set("Vous pouvez chercher le mot.")
                self.varLabel2.set("GO !")

                self.initialisation_mot_a_trouver()
            
            # affichage plusieurs joueur

            else:
                self.message("")
                self.varLabel2.set("Un joueur va entrer un mot que les autres joueurs devront trouver.\nLe mot ne doit pas contenir de charactère spécial comme par exemple 'é'.")


    def initialisation_mot_a_trouver (self):


        if self.nbr_de_joueur == 1:

            self.mot_a_trouver = choice(self.list_mots)
            self.mot_conversion()

        if self.nbr_de_joueur == 2:

            self.reponse_donnee()

            self.varLabel2.set("Un joueur va entrer un mot que les autres joueurs devront trouver.\nLe mot ne doit pas contenir de charactère spécial comme par exemple 'é'.")
            self.mot_a_trouver = self.entree

            for i in range (0, len(self.mot_a_trouver)) :
                
                if self.mot_a_trouver[i] in self.value_error_mot : 
                    self.error = True
                
            if self.error == True :

                self.message("Erreur d'entrée! (" + self.mot_a_trouver + ")")
                self.mot_a_trouver = ''
                self.error = False
            
            else:
                self.message("")
                self.varLabel1.set("Les autres joueurs peuvent chercher le mot.")
                self.varLabel2.set("GO !")
                self.mot_conversion()


    def mot_conversion (self):
        
        self.mot_a_trouver = self.mot_a_trouver.lower()

        for i in range (0, len(self.mot_a_trouver)):
            self.mot_cache.append("*")
            

    def tour_joueur(self):

        self.reponse_donnee()
        
        self.mot_cache_affiche = ''
        self.lettre_proposee_dans_mot = False
        self.error = False

        self.lettre_proposee = self.entree

        if len(self.lettre_proposee) != 1 :
                
            self.error = True

        for i in range (0, len(self.mot_a_trouver)) :

            if self.mot_a_trouver[i] in self.value_error_mot :

                self.error = True

            if self.error == True:

                self.message("Entrée incompatible. (" + self.lettre_proposee + ")")
                return

            if self.lettre_proposee not in self.lettrre_entree :

                if self.mot_a_trouver[i] == self.lettre_proposee :
                        
                    self.mot_cache.pop(i)
                    self.mot_cache.insert(i, self.lettre_proposee)
        
                    self.lettre_proposee_dans_mot = True

            else : 

                self.lettre_proposee_dans_mot = True

        for i in range (0, len(self.mot_a_trouver)):

            self.mot_cache_affiche = self.mot_cache_affiche + self.mot_cache[i]
        
        if self.lettre_proposee_dans_mot == True :

            if self.lettre_proposee in self.lettrre_entree :
                self.message("La lettre est déja dans le mot. (" + self.lettre_proposee + ")\n" + self.mot_cache_affiche)

            else :
                self.lettrre_entree.append(self.lettre_proposee)
                self.message(self.mot_cache_affiche)


        else :
            if self.lettre_proposee not in self.lettrre_entree :

                self.lettrre_entree.append(self.lettre_proposee)
                self.nbr_erreur += 1
                self.message(str("{} n'est pas dans le mot.\nIl vous reste {} chance(s).\n{}").format(self.lettre_proposee, str(self.nbr_erreur_max - self.nbr_erreur), self.mot_cache_affiche))

            else :

                self.message("Vous avez déjà essayer cette lettre. (" + self.lettre_proposee + ")\n" + self.mot_cache_affiche)

        if self.nbr_erreur == self.nbr_erreur_max :

            self.varLabel1.set("Dommage c'est perdu.")
            self.varLabel2.set("Le mot était " + self.mot_a_trouver)
            self.gagne = "Perdu"

            if self.nbr_de_joueur == 2 :
                
                self.message("Voulez-vous ajouter le mot à la liste des mots pour le mode 1 joueur."+ "\nEntrer 1 pour l'ajouter et 2 pour passer.")

            else :
                
                self.varButton.set("Rejouer")
                self.message("Cliquer sur rejouer pour rejouer.\nSinon fermez juste la fenêtre.")

        if self.mot_a_trouver == self.mot_cache_affiche :

            self.varLabel1.set("Bravo vous avez trouver ! Le mot était " + self.mot_a_trouver)
            self.varLabel2.set("Vous avez trouver avec " + str(self.nbr_erreur) + " faute(s).")
            self.gagne = "Gagné"

            if self.nbr_de_joueur == 2 :
                
                self.message("Voulez-vous ajouter le mot à la liste des mots pour le mode 1 joueur."+ "\nEntrer 1 pour l'ajouter et 2 pour passer.")

            else :
                
                self.varButton.set("Rejouer")
                self.message("Cliquer sur rejouer pour rejouer.\nSinon fermez juste la fenêtre.")

        
    def ask_mot_ajoute (self) :
        
        self.reponse_donnee()

        try:
            self.entree = int(self.entree)

        # si impossible d'obtenir la valeur int() à partir d'entrée, renvoye un erreur

        except ValueError:
            self.message("Erreur d'entrée! (" + self.entree[0:10] + ")" + "\nVoulez-vous ajouter le mot à la liste des mots pour le mode 1 joueur."+ "\nEntrer 1 pour l'ajouter et 2 pour passer.")

        if self.entree not in self.range_nbr_joueur :

            self.message("Erreur d'entrée! (" + self.entree[0:10] + ")")
            return

        if self.entree == 1 :

            if self.mot_a_trouver in self.list_mots :
                self.message("Le mot était déjà dans la liste.")

            else :
                
                self.message("Le mot a bien été ajouté.")
                self.list_mots.append(self.mot_a_trouver)
            
            with open('C:/Users/Arcarox/Desktop/Mes Documents/ISN/Pour aller plus loin/pendu/donnees.json', 'w') as file :
                file.write(json.dumps({'list_mots' :self.list_mots}, indent= 4))

            self.donnees_file.closed
            self.mot_ajoute = "Oui"

        else : 
            self.mot_ajoute = "Non"

        self.varButton.set("Rejouer")
        self.message("Cliquer sur rejouer pour rejouer.\nSinon fermez juste la fenêtre.")


    def fonction_rejouer (self) :
                
        self.initialisation_valeur()


    def reponse_donnee (self) :

        #récupère les valeurs dans Entry

        # si l'entrée est vide : fin de la fonction

        if self.reponse.get() == "":
            return

        # essaye d'obtenir une valeur int() de l'entrée

        try:
            self.entree = (self.reponse.get())

        # si impossible d'obtenir la valeur int() à partir d'entrée, renvoye un erreur

        except ValueError:
            self.message("Erreur d'entrée! (" + self.reponse.get()[0:10] + ")")
            self.reponse.delete(0, 'end')

        # clear la zone d'entrée
        else:
            self.reponse.delete(0, 'end')


    def message (self, message) : 

        #Supprime le contenu de la boite txt et affiche le message donné

        self.results_txt.delete(0.0, END)
        self.results_txt.insert(0.0, message)



def main () :


    root = Tk()
    root.title("Pendu")
    app = Application(root)
    root.mainloop()
    app.mainloop()


main()