from random import choice
import json

class Application ():

    def __init__ (self):

        self.jeu()


    def jeu(self):

        self.initialisation_valeur()
        self.initialisation_nbr_joueur()
        self.intitialisation_difficulte()
        self.initialisation_mot_a_trouver()
        self.mot_conversion()
        self.tour_joueur()

    def initialisation_valeur (self) :

        self.nbr_de_joueur = ''
        self.range_nbr_joueur = ['1', '2']
        self.difficulte = ""
        self.nbr_erreur_max = 0
        self.list_nbr_erreur_max = [4, 8, 10]
        self.range_difficulte = ['1', '2', '3']
        self.value_error_mot = ['1','2','3','4','5','6','7','8','9',' ']
        self.error = False
        self.mot_a_trouver = ''
        self.donnees_file = open('C:/Users/Arcarox/Desktop/Mes Documents/ISN/Pour aller plus loin/pendu/donnees.json', 'r')
        self.data = json.load(self.donnees_file)
        self.list_mots = self.data['list_mots']
        self.donnees_file.closed
        self.mot_cache = list()
        self.lettre_proposee = ""
        self.avancement_mot_trouve = list()
        self.lettre_proposee_dans_mot = False
        self.nbr_erreur = 0
        self.mot_trouve = False
        self.mot_cache_affiche = ''
        self.rejouer = 0
        self.mot_ajoute = ''


    def initialisation_nbr_joueur (self):
    
        print ("Veuillez définir si vous jouer seul ou à plusieurs.\n")

        while self.nbr_de_joueur == '' :

            self.nbr_de_joueur = input ("nombre de joueur ? Tapez 1 ou 2 .\n")

            if self.nbr_de_joueur not in self.range_nbr_joueur:

                print ("Nombre de joueur incorrect. Veuillez recommencer.\n")
            
            else:
                self.nbr_de_joueur = int(self.nbr_de_joueur)




    def intitialisation_difficulte (self) :        
        

        while self.difficulte == "":
        
            print ("Veuillez definir une difficult:\n")
            print ("Entrer 1 pour avoir une limite de 4 erreurs.")
            print ("Entrer 2 pour avoir une limite de 8 erreurs.")
            print ("Entrer 3 pour avoir une limite de 10 erreurs.")

            self.difficulte = input("\nChoisissez une limite : ")

            if self.difficulte not in self.range_difficulte:
                                                
                print ("\nVeuillez rentrer une limite proposée\n")
                self.difficulte = ""

        self.nbr_erreur_max = self.list_nbr_erreur_max[int(self.difficulte)-1]


    def initialisation_mot_a_trouver (self):


        if self.nbr_de_joueur == 1:

            self.mot_a_trouver = choice(self.list_mots)

        if self.nbr_de_joueur == 2:

            self.mot_a_trouver = input("Entrer le mot que les autres joueurs devront trouver: ")

            for i in range (0, len(self.mot_a_trouver)) :
                
                if self.mot_a_trouver[i] in self.value_error_mot : 
                    self.error = True
                
                if self.error == True :

                    self.initialisation_mot_a_trouver()

    def mot_conversion (self):
        
        self.mot_a_trouver = self.mot_a_trouver.lower()

        for i in range (0, len(self.mot_a_trouver)):
            self.mot_cache.append("*")
            self.avancement_mot_trouve.append("*")


    def tour_joueur(self):

        while self.mot_cache_affiche != self.mot_a_trouver or self.nbr_erreur == 0:
            
            self.mot_cache_affiche =""
            self.lettre_proposee_dans_mot = False
            self.error = False        
            self.lettre_proposee = input("Entrer une lettre: ")

            if len(self.lettre_proposee) > 1 or len(self.lettre_proposee) < 1 :
                
                self.error = True

            for i in range (0, len(self.mot_a_trouver)) :

                if self.mot_a_trouver[i] in self.value_error_mot :

                    self.error = True

                if self.error == True:

                    print("Entrée incompatible.")
                    self.tour_joueur()

                if self.mot_a_trouver[i] == self.lettre_proposee :
                    
                    #self.avancement_mot_a_trouve.pop(i)
                    #self.avancement_mot_a_trouve.insert(i, self.lettre_proposee)

                    self.mot_cache.pop(i)
                    self.mot_cache.insert(i, self.lettre_proposee)
    
                    self.lettre_proposee_dans_mot = True

            if self.lettre_proposee_dans_mot == True :

                for i in range (0, len(self.mot_a_trouver)):

                    self.mot_cache_affiche = self.mot_cache_affiche + self.mot_cache[i]

                print (self.mot_cache_affiche)


            else :
                self.nbr_erreur += 1
                print ('"' + self.lettre_proposee + '"' + " n'est pas dans le mot.")
                print ("Il vous reste " + str(self.nbr_erreur_max - self.nbr_erreur) + " chance(s).")
                print (self.mot_cache_affiche)

        if self.nbr_erreur == 0 :
            print("Dommage c'est perdu. Le mot était " + self.mot_a_trouver)

        else :
            print ("Bravo vous avez trouvé avec " + str(self.nbr_erreur) + " fautes.")


        print ("Voulez-vous ajouter le mot à la liste des mots pour le mode 1 joueur.")
        self.mot_ajoute = input("Entrer 1 pour l'ajouter et 2 pour passer: ")

        if self.mot_ajoute == '1' :

            if self.mot_a_trouver in self.list_mots :
                print("Le mot était déjà dans la liste.")

            else :
                
                print("Le mot a bien été ajouté.")
                self.list_mots.append(self.mot_a_trouver)
            
            with open('donnees.json', 'w') as file :
                file.write(json.dumps({'list_mots' :self.list_mots}, indent= 4))

            self.donnees_file.closed
        

        self.fonction_rejouer()


    def fonction_rejouer (self) :

        print("Si vous voulez rejouer entrer 1, sinon entrer 2.")
        self.rejouer = input("")

        if self.rejouer == '1':
            self.jeu()
        
            




def main () :

    app = Application()
    


if __name__ == "__main__":
    main()