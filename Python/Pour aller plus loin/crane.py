# But de l'application : Identifier l'espece qui correpond à un fossile de crane dont on connait les caracteristique
# Pb : Fait à partir de documents donc fiabilite bassee sur ces documents

from tkinter import *

# Class pour recuperer les caracteristique du crane
class recup_infos (Frame) :

    # initialisation des valeurs et de la fenêtre
    def __init__ (self, master):

        Frame.__init__(self,master)
        master.minsize(width= 500, height = 700)
        self.grid()

        self.V_cra = 0
        self.prognathisme = ""
        self.relief_face = ""
        self.front = ""
        self.bourrelets_sus_orbitaires = ""
        self.forme_arriere_du_crane = ""
        self.dev_arcades_zygomatiques = ""
        self.crete_sagittale = ""
        self.crete_occipitale = ""
        self.h_voute_cra = ""
        self.dentition = ""
        self.orbites = ""
        self.menton = ""

        self.init_widjet()

    #initialisation des elements de la fenetre
    def init_widjet (self):

        #Titre
        Label(self, text = 'Entrer vos observations').grid(row = 0, sticky = W)

        #Volume cranien
        Label(self, text = 'Volume cranien :').grid(row = 1, sticky = W)

        self.v_entry = Entry(self)
        self.v_entry.grid(row = 1, sticky = E)

        #Prognathisme
        Label(self, text = 'Prognathisme :').grid(row = 2, sticky = W)

        self.prognathisme_entry_var = StringVar()
        self.prognathisme_entry_var.set(0)
        text_radio = ['Tres eleve', 'eleve', 'moyen', 'aucun']
        for i in range (4) :
            self.prognathisme_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.prognathisme_entry_var)
            self.prognathisme_entry.grid(row = 3, column = i)

        #Front
        Label(self, text = 'Front :').grid(row = 4, sticky = W)

        self.front_entry_var = StringVar()
        self.front_entry_var.set(0)
        text_radio = ['fuyant', 'moyennement redresse', 'droit']
        for i in range (3):
            self.front_entry  = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.front_entry_var)
            self.front_entry.grid(row = 5, column = i)

        #Relief de la face
        Label(self, text = 'Relief de la face :').grid(row = 6, sticky = W)

        self.relief_face_entry_var = StringVar()
        self.relief_face_entry_var.set(0)
        text_radio = ['soucoupe', 'incurve verticalement', 'plat']
        for i in range (3):
            self.relief_face_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.relief_face_entry_var)
            self.relief_face_entry.grid(row = 7, column = i)

        #Bourrelets sus-orbitaires
        Label(self, text = 'Bourrelets sus-orbitaires :').grid(row = 8, sticky = W)

        self.bourrelets_sus_orbitaires_entry_var = StringVar()
        self.bourrelets_sus_orbitaires_entry_var.set(0)
        text_radio = ['aucun', 'moyen', 'tres marque']
        for i in range (3):
            self.bourrelets_sus_orbitaires_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.bourrelets_sus_orbitaires_entry_var)
            self.bourrelets_sus_orbitaires_entry.grid(row = 9, column = i)

        #Forme des orbites
        Label(self, text = 'Forme des orbites :').grid(row = 10, sticky = W)

        self.orbites_entry_var = StringVar()
        self.orbites_entry_var.set(0)
        text_radio = ['rectangulaire', 'arrondi']
        for i in range (2):
            self.orbites_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.orbites_entry_var)
            self.orbites_entry.grid(row = 11, column = i)

        #Forme de l'arriere du crane
        Label(self, text = 'Forme arriere du crane :').grid(row = 12, sticky = W)

        self.forme_arriere_du_crane_entry_var = StringVar()
        self.forme_arriere_du_crane_entry_var.set(0)
        text_radio = ['arrondi forme de maison', 'arrondi, de forme ovale', 'anguleux', 'tres anguleux', 'tres anguleux forme de maison', 'tres anguleux forme ovale']
        for i in range (6):
            self.forme_arriere_du_crane_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.forme_arriere_du_crane_entry_var)
            self.forme_arriere_du_crane_entry.grid(row = 13, column = i)

        #Developpement des arcades zygomatiques
        Label(self, text = 'Developpement des arcades zygomatiques :').grid(row = 14, sticky = W)

        self.dev_arcades_zygomatiques_entry_var = StringVar()
        self.dev_arcades_zygomatiques_entry_var.set(0)
        text_radio = ['tres reduite', 'peu ecartees', 'fines et moyennement ecartees',' larges et tres ecartees', 'fines et peu ecartees']
        for i in range (5):
            self.dev_arcades_zygomatiques_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.dev_arcades_zygomatiques_entry_var)
            self.dev_arcades_zygomatiques_entry.grid(row = 15, column = i)

        #Presence de la crete saggitale
        Label(self, text = 'Presence de la crete saggitale :').grid(row = 16, sticky = W)

        self.crete_sagittale_entry_var = StringVar()
        self.crete_sagittale_entry_var.set(0)
        text_radio = ['non', 'oui']
        for i in range (2):
            self.crete_sagittale_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.crete_sagittale_entry_var)
            self.crete_sagittale_entry.grid(row = 17, column = i)

        #Presence de la crete occipitale
        Label(self, text = 'Presence de la crete occipitale :').grid(row = 18, sticky = W)

        self.crete_occipital_entry_var = StringVar()
        self.crete_occipital_entry_var.set(0)
        text_radio = ['non', 'oui']
        for i in range (2):
            self.crete_occipital_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.crete_occipital_entry_var)
            self.crete_occipital_entry.grid(row = 19, column = i)

        #Hauteur de la voute cranienne
        Label(self, text = 'Hauteur de la voute cranienne :').grid(row = 20, sticky = W)

        self.h_voute_cra_entry_var = StringVar()
        self.h_voute_cra_entry_var.set(0)
        text_radio = ['elevee', 'moyenne', 'basse']
        for i in range (3):
            self.h_voute_cra_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.h_voute_cra_entry_var)
            self.h_voute_cra_entry.grid(row = 21, column = i)

        #Dentition
        Label(self, text = 'Dentition :').grid(row = 22, sticky = W)

        self.dentition_entry_var = StringVar()
        self.dentition_entry_var.set(0)
        text_radio = ['parabolique', 'grosse molaire']
        for i in range (2):
            self.dentition_entry = Radiobutton(self, text = text_radio[i], value = text_radio[i], variable = self.dentition_entry_var)
            self.dentition_entry.grid(row = 23, column = i)

        #Menton
        Label(self, text = 'Menton :').grid(row = 24, sticky = W)

        self.menton_entry_var = StringVar()
        self.menton_entry_var.set(0)
        self.menton_entry = Checkbutton(self, text = 'oui', variable = self.menton_entry_var, onvalue = 'oui', offvalue = '')
        self.menton_entry.grid(row = 24)

        #Bouton validation qui lance la recuperation des infos
        self.Button1 = Button(self, text = 'Valider')
        self.Button1.grid(row=25)
        self.Button1.bind('<Button-1>', self.take_element)

    # fonction qui recupere les entrees de l'utilisateur
    def take_element(self, Event):

        #Volume cranien qui est une var(int)
        try :
            self.V_cra = int(self.v_entry.get())
        except :
            self.V_cra = 0

        self.prognathisme = self.prognathisme_entry_var.get()
        
        self.front = self.front_entry_var.get()

        self.relief_face = self.relief_face_entry_var.get()

        self.bourrelets_sus_orbitaires = self.bourrelets_sus_orbitaires_entry_var.get()

        self.orbites = self.orbites_entry_var.get()
        
        self.forme_arriere_du_crane = self.forme_arriere_du_crane_entry_var.get()

        self.dev_arcades_zygomatiques = self.dev_arcades_zygomatiques_entry_var.get()

        self.crete_sagittale = self.crete_sagittale_entry_var.get()
        
        self.crete_occipitale = self.crete_occipital_entry_var.get()

        self.h_voute_cra = self.h_voute_cra_entry_var.get()

        self.dentition = self.dentition_entry_var.get()

        self.menton = self.menton_entry_var.get()

        #quit la loop de la fenetre pour pouvoir acceder au reste
        global infos_frame 
        infos_frame.quit()

        

# Class pour identifier l'espece correspondante au caracteristique recuperer
class espece(recup_infos, Frame) :

    # initialisation des valeurs et de la fenetre
    def __init__ (self, infos, master) :

        Frame.__init__(self, master)
        master.minsize(width = 100, height= 50)
        self.grid()

        self.paranthropus = 0
        self.australopithecus = 0
        self.homo_habilis = 0
        self.homo_erectus = 0
        self.homo_neanderthalensis = 0
        self.homo_sapiens = 0
        self.classement = []
        self.correspondance = []
        self.results = StringVar()
        self.results.set("")

        self.init_widjet()

        self.test(infos)

    #initialisation des elements de la fenetre
    def init_widjet (self):

        Label(self, text = "Les caracteristiques entrees correspondent à un crane pouvant appartenir à l'espece :").grid(row = 0, sticky = W)

    #appel de toute les fonctions test de correpondance
    def test (self, infos):

        self.test_cra(infos)
        self.test_prognathisme(infos)
        self.test_front(infos)
        self.test_relief_face(infos)
        self.test_bourrelets_sus_orbitaires(infos)
        self.test_forme_arriere_du_crane(infos)
        self.test_dev_arcades_zygomatiques(infos)
        self.test_orbites(infos)
        self.test_crete_sagittale(infos)
        self.test_crete_occipitale(infos)
        self.test_h_voute_cra(infos)
        self.test_dentition(infos)
        self.test_menton(infos)

        self.calcule_correspondance()

    # test correspondance volume cranien
    def test_cra (self, infos):

        if infos.V_cra > 300 and infos.V_cra < 500 :
            self.australopithecus += 1
        
        if infos.V_cra > 450 and infos.V_cra < 600 :
            self.paranthropus += 1

        if infos.V_cra > 500 and infos.V_cra < 680 :
            self.homo_habilis += 1

        if infos.V_cra >  800 and infos.V_cra < 1300 :
            self.homo_erectus += 1

        if infos.V_cra > 1400 and infos.V_cra < 1500 :
            self.homo_sapiens += 1
        
        if infos.V_cra > 1300 and infos.V_cra < 1600 :
            self.homo_neanderthalensis += 1

    # test correspondance prognathisme
    def test_prognathisme (self, infos):

        if infos.prognathisme == 'tres eleve' :
            self.australopithecus += 1

        if infos.prognathisme == 'eleve':
            self.paranthropus += 1
            self.homo_habilis += 1

        if infos.prognathisme == 'moyen':
            self.homo_neanderthalensis += 1
            self.homo_erectus += 1

        if infos.prognathisme == 'aucun':
            self.homo_sapiens += 1

    # test correspondance front
    def test_front (self, infos):

        if infos.front == 'fuyant':
            self.australopithecus += 1
            self.paranthropus += 1
            self.homo_neanderthalensis += 1
            self.homo_habilis

        if infos.front == 'moyenne redresse':
            self.homo_erectus += 1

        if infos.front == 'droit' :
            self.homo_sapiens += 1

    # test correspondance relief de la face
    def test_relief_face (self, infos):

        if infos.relief_face == 'soucoupe':
            self.paranthropus += 1

        if infos.relief_face == 'incurve vertical':
            self.australopithecus += 1

        if infos.relief_face == 'plat':
            self.homo_habilis += 1

    # test correspondance bourrelets sus-orbitaires
    def test_bourrelets_sus_orbitaires (self, infos):
        
        if infos.bourrelets_sus_orbitaires == 'aucun':
            self.homo_sapiens += 1

        if infos.bourrelets_sus_orbitaires == 'moyen':
            self.homo_neanderthalensis += 1
            self.australopithecus += 1
            self.paranthropus += 1
            self.homo_habilis += 1
        
        if infos.bourrelets_sus_orbitaires == 'tres marque':
            self.homo_erectus += 1
    
    # test correspondance forme des orbites
    def test_orbites (self, infos):

        if infos.orbites == 'rectangulaires':
            self.homo_sapiens += 1

        if infos.orbites == 'arrondies':
            self.homo_neanderthalensis += 1

    # test correspondance forme arriere du crane
    def test_forme_arriere_du_crane (self, infos) :

        if infos.forme_arriere_du_crane == 'arrondi forme de maison':
            self.homo_sapiens += 1
        
        if infos.forme_arriere_du_crane == 'arrondi, de forme ovale':
            self.homo_neanderthalensis += 1

        if infos.forme_arriere_du_crane == 'anguleux':
            self.australopithecus += 1

        if infos.forme_arriere_du_crane == 'tres anguleux':
            self.paranthropus += 1

        if infos.forme_arriere_du_crane == 'tres anguleux forme de maison':
            self.homo_erectus += 1
        
        if infos.forme_arriere_du_crane == 'tres anguleux forme ovale':
            self.homo_habilis += 1

    # test correspondance developpement des arcades zygomatiques
    def test_dev_arcades_zygomatiques (self, infos) :

        if infos.dev_arcades_zygomatiques == 'tres reduites':
            self.homo_sapiens += 1

        if infos.dev_arcades_zygomatiques == 'peu ecartees':
            self.homo_neanderthalensis += 1

        if infos.dev_arcades_zygomatiques == 'fines et moyennement ecartees':
            self.australopithecus += 1

        if infos.dev_arcades_zygomatiques == 'larges et tres ecartees':
            self.paranthropus += 1
            self.homo_erectus += 1

        if infos.dev_arcades_zygomatiques == 'fines et peu ecartees':
            self.homo_habilis += 1

    # test correspondance crete sagitalle
    def test_crete_sagittale (self, infos):

        if infos.crete_sagittale == 'non':
            self.homo_sapiens += 1
            self.homo_neanderthalensis += 1
            self.australopithecus += 1
            self.homo_habilis += 1
        
        if infos.crete_sagittale == 'oui':
            self.paranthropus += 1
            self.homo_erectus += 1

    # test correspondance crete occipitale
    def test_crete_occipitale (self, infos):

        if infos.crete_occipitale == 'non':
            self.homo_sapiens += 1
            self.homo_habilis += 1

        if infos.crete_occipitale == 'oui':
            self.homo_neanderthalensis += 1
            self.paranthropus += 1
            self.australopithecus += 1
            self.homo_erectus += 1

    # test correspondance hauteur de la voute cranienne
    def test_h_voute_cra (self, infos):

        if infos.h_voute_cra == 'elevee':
            self.homo_sapiens += 1

        if infos.h_voute_cra == 'moyenne':
            self.homo_neanderthalensis += 1

        if infos.h_voute_cra == 'basse':
            self.paranthropus += 1
            self.australopithecus += 1
            self.homo_erectus += 1
            self.homo_habilis += 1

    # test correspondance dentition
    def test_dentition (self, infos):

        if infos.dentition == 'parabolique':
            self.homo_sapiens += 1
            self.homo_erectus += 1
            self.australopithecus += 1

        if infos.dentition == 'grosse molaire':
            self.paranthropus += 1

    # test correspondance menton
    def test_menton (self, infos):

        if infos.menton == 'oui':
            self.homo_sapiens += 1

    # calcul pourcentage de correpondance pour chaque espece, les classe du plus grand au plus petit, fait une liste de ceux qui ont le pourcentage de correspondance le plus haut
    def calcule_correspondance (self) :

        # calcule des pourcentages
        self.classement.append(self.homo_sapiens / 12)
        self.classement.append(self.homo_neanderthalensis / 10)
        self.classement.append(self.homo_erectus / 10)
        self.classement.append(self.homo_habilis / 10)
        self.classement.append(self.paranthropus / 11)
        self.classement.append(self.australopithecus / 11)

        # classement du plus grand au plus petit
        self.classement.sort(reverse = TRUE)

        # garde seulement ceux qui ont le plus grand pourcentage de correspondance
        if self.homo_erectus / 10 == self.classement[0] :
            self.correspondance.append('homo erectus')

        if self.homo_sapiens / 12 == self.classement[0] :
            self.correspondance.append('homo sapiens')

        if self.homo_neanderthalensis / 10 == self.classement[0] :
            self.correspondance.append('homo neanderthalensis')

        if self.homo_habilis / 10 == self.classement[0] :
            self.correspondance.append('homo habilis')

        if self.paranthropus / 11 == self.classement[0] :
            self.correspondance.append('paranthropus')

        if self.australopithecus / 11 == self.classement[0] :
            self.correspondance.append('australopithecus')

        self.affiche_results()

    # affiche les resultats dans la nouvelle fenetre
    def affiche_results (self) :

        for i in range (0, len(self.correspondance)):
            print(self.correspondance[i])
            Label(self, text = '- {0}\n'.format(self.correspondance[i])).grid(row = i + 1, sticky = W)



# Prevention contre un appel depuis un autre fichier
if __name__ == '__main__':

    # Création de la premiere fenetre et appel de la premiere class
    infos_frame = Tk()
    infos_frame.title('Observation')
    infos_recup = recup_infos(infos_frame)
    infos_frame.mainloop()

    # Création de la seconde fenetre et appel de la deuxieme class
    result_frame = Tk()
    result_frame.title("Resultats")
    classement = espece(infos_recup, result_frame)
    result_frame.mainloop()
