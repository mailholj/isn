# -*- coding: utf-8 -*-

class u ():

    def __init__ (self,):

        self.init_values()
    
    def init_values(self):
        
        print('''u(n) est definit comme :\n
        Si u(n) = 1, alors la suite est finie et u(n) est son dernier élément.\n
        Si u(n) est pair, alors u(n+1) = u(n) / 2 .\n
        Si u(n) est impair, alors u(n+1) = 3 x u(n) + 1 .''')
        
        self.k = 0
        self.r = 1000
        self.un()

    def  un (self):

        while self.k == 0 :

            try :
                self.k = int(input("Entrer une valeur de n>= 1 :"))

            except :
                print("Erreur d'entrée.\n")

        self.cal_un()

        if self.r == 1 :
            print("La suite est finie à partir de n = {0} car u({0}) = {1} .".format(str(self.n), str(self.r)))            
        
        else :
            print("Le terme d'indice {0} est égale à {1} !".format(str(self.n), str(self.r)))


    def cal_un(self) :

        for self.n in range (1, self.k +1) :

            if self.r % 2 == 0 :
                self.r = self.r / 2
            
            else :
                self.r = self.r * 3 +1

            if self.r == 1 :
                return

u()