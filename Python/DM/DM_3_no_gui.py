# -*- coding: utf-8 -*-

class u ():
    #class qui comprend le programme entier

    def __init__ (self,):
        #fonction appelée à l'initialisation de la classe

        self.init_values()
    
    def init_values(self):
        #initialisation des valeurs et description de la suite u(n) pour l'utilisateur
        
        print('''u(n) est definit comme :\n
        Si u(n) = 1, alors la suite est finie et u(n) est son dernier élément.\n
        Si u(n) est pair, alors u(n+1) = u(n) / 2 .\n
        Si u(n) est impair, alors u(n+1) = 3 x u(n) + 1 .''')
        
        self.k = 0
        self.r = 1000
        self.un()

    def  un (self):
        #Fonction principale

        #On demande k tant qu'il n'a pas était modifié

        while self.k == 0 :
            #Prévention contre les erreurs d'entrées

            try :
                self.k = int(input("Entrer une valeur de n>= 1 :"))

            except :
                print("Erreur d'entrée.\n")

        #On calcule U(k), on l'affiche à l'utilisateur

        self.cal_un()

        if self.r == 1 :
            #Si la suite est finie on indique à l'utilisateur que la suite l'est
            print("La suite est finie à partir de n = {0} car u({0}) = {1} .".format(str(self.n), str(self.r)))            
        
        else :
            #Sinon on lui affiche la valeur de u(n) demandée
            print("Le terme d'indice {0} est égale à {1} !".format(str(self.n), str(self.r)))


    def cal_un(self) :
        #fonction calcule le terme de u à l'indice k

        for self.n in range (1, self.k +1) :

            if self.r % 2 == 0 :
                self.r = self.r / 2
            
            else :
                self.r = self.r * 3 +1

            if self.r == 1 :
                return
          
#Pour les potentielles import depuis d'autre fichier 
if __name__ == '__main__' :

    u()