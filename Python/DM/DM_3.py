# -*- coding: utf-8 -*-

from tkinter import *


class u (Frame):

    def __init__ (self, master):
        Frame.__init__(self, master)
        master.minsize(width= 500, height= 200)
        self.grid()

        self.creation_widjet()
        self.init_values()

    def creation_widjet(self):

        #création des éléments de la fenêtre

        self.varLabel1 = StringVar()
        Label(self, justify = LEFT,textvariable = self.varLabel1).grid(row = 0, column = 0, columnspan = 2, sticky = W)

        self.varLabel2 = StringVar()
        Label(self,textvariable = self.varLabel2).grid(row = 1, column = 0, columnspan = 2, sticky = W)

        self.reponse = Entry(self)
        self.reponse.grid(row=2, column=1, sticky=W)

        self.varButton = StringVar()
        self.Button1 = Button(self, textvariable = self.varButton)
        self.Button1.grid(row=2, column=2, columnspan=4, sticky=W)
        self.Button1.bind('<Button-1>', self.entry_logic)

        # permet l'usage de la touche entrée

        self.master.bind('<Return>', self.entry_logic)

        self.results_txt = Text(self, width=40, height=5,wrap=WORD)
        self.results_txt.grid(row = 3, column = 0, columnspan = 4)


    def init_values(self):

        self.varButton.set("Entrer")
        self.varLabel1.set("u(n) est definit comme :\nSi u(n) = 1, alors la suite est finie et u(n) est son dernier élément.\nSi u(n) est pair, alors u(n+1) = u(n) / 2 .\nSi u(n) est impair, alors u(n+1) = 3 x u(n) + 1 .")
        self.varLabel2.set("Entrer une valeur de n>= 1 :")


        self.k = 0
        self.entree = ""
        self.pair = bool
        self.r = 1000
        

    def entry_logic (self, Event):

        if self.k == 0 :
            self.ask_k()         

    def ask_k (self):
        
        self.reponse_donnee()
 
        
        self.k = self.entree


        n = self.un()

        self.message("Le terme d'indice " + str(int(n)) + " est égale à " + str(self.r) + " !")

        self.init_values()
    
    def un(self) :

        for n in range (1, self.k +1) :

            if n % 2 == 0 :
                self.r = self.r / 2
            
            else :
                self.r = self.r * 3 +1

            if self.r == 0 :
                return n

        return self.k
          


    def reponse_donnee (self) :

        #récupère les valeurs dans Entry

        # si l'entrée est vide : fin de la fonction

        if self.reponse.get() == "":
            return

        # essaye d'obtenir une valeur int() de l'entrée

        try:
            self.entree = int((self.reponse.get()))

        # si impossible d'obtenir la valeur int() à partir d'entrée, renvoye un erreur

        except ValueError:
            self.message("Erreur d'entrée! (" + self.reponse.get()[0:10] + ")")
            self.reponse.delete(0, 'end')

        # clear la zone d'entrée
        else:
            self.reponse.delete(0, 'end')

    def message(self, message):

        self.results_txt.delete(0.0, END)
        self.results_txt.insert(0.0, message)


def main():
    root = Tk()
    root.title("DM 3")
    suite = u(root)
    root.mainloop()
    suite.mainloop()

main()